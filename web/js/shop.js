$('.add_to_cart').on('submit', function (e) {
    var quantity = $(this).find('.quantity').val(),
        url = $(this).attr('action');

    $.ajax({
        url: url,
        type: "POST",
        dataType:"json",
        data: {
            'quantity': quantity
        }
    }).done(function(data){
        location.reload();
    });

    e.preventDefault();
});

$('.category-picker').on('change', function (e) {
    var category = $("option:selected", this).val();

    if (category != 0) {
        window.location.href = document.location.pathname + '?category=' + category;
    } else {
        window.location.href = document.location.pathname;
    }
});