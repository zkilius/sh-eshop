<?php

namespace Tests\Shop\ShopBundle\Manager;

use Doctrine\ORM\EntityManager;
use Shop\ShopBundle\Entity\Cart;
use Shop\ShopBundle\Entity\CartItem;
use Shop\ShopBundle\Entity\Product;
use Shop\ShopBundle\Manager\CartManager;
use Shop\UserBundle\Entity\User;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Tests\Shop\Mocks\FakeToken;

class CartManagerTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var CartManager
     */
    private $manager;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $em;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $tokenStorage;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $session;

    public function setUp()
    {
        $this->em = $this->getMockBuilder(EntityManager::class)->disableOriginalConstructor()->getMock();
        $this->tokenStorage = $this->getMockBuilder(TokenStorage::class)->disableOriginalConstructor()->getMock();
        $this->session = $this->getMockBuilder(Session::class)->disableOriginalConstructor()->getMock();

        $this->manager = new CartManager($this->em, $this->tokenStorage, $this->session);
    }

    /**
     * @test
     */
    public function should_create_empty_basket_if_session_has_none()
    {
        $this->session->expects($this->once())->method('set')->with('cart', new Cart());

        $this->manager->getCart();
    }

    /**
     * @test
     */
    public function should_insert_item_into_cart()
    {
        $this->session->expects($this->exactly(4))->method('get')->with('cart')->willReturn(new Cart());
        $this->tokenStorage->expects($this->once())->method('getToken')->willReturn(new FakeToken(new User()));

        $cart = $this->manager->addItemToCart(new Product(), 1);
        $items = $cart->getItems();

        $this->assertInstanceOf(Cart::class, $cart);
        $this->assertCount(1, $items);
        $this->assertEquals(1, reset($items)->getQuantity());
    }

    /**
     * @test
     */
    public function should_update_item_in_cart()
    {
        $cart = new Cart();

        $product = new Product();

        $cartItem = new CartItem();
        $cartItem->setProduct($product);
        $cartItem->setQuantity(1);

        $cart->addItem($cartItem);

        $this->session->expects($this->exactly(5))->method('get')->with('cart')->willReturn($cart);
        $this->tokenStorage->expects($this->once())->method('getToken')->willReturn(new FakeToken(new User()));

        $cart = $this->manager->addItemToCart($product, 2);
        $items = $cart->getItems();

        $this->assertInstanceOf(Cart::class, $cart);
        $this->assertCount(1, $items);
        $this->assertEquals(3, reset($items)->getQuantity());
    }

    /**
     * @test
     */
    public function should_calculate_total_correctly()
    {
        $cart = new Cart();

        $product = new Product();
        $product->setId(1);
        $product->setPrice(15);

        $product2 = new Product();
        $product2->setId(2);
        $product2->setPrice(10);

        $cartItem = new CartItem();
        $cartItem->setProduct($product);
        $cartItem->setQuantity(1);

        $cart->addItem($cartItem);

        $this->session->expects($this->exactly(4))->method('get')->with('cart')->willReturn($cart);
        $this->tokenStorage->expects($this->once())->method('getToken')->willReturn(new FakeToken(new User()));

        $cart = $this->manager->addItemToCart($product2, 2);
        $items = $cart->getItems();

        $this->assertInstanceOf(Cart::class, $cart);
        $this->assertCount(2, $items);
        $this->assertEquals(35, $cart->getTotal());
    }

    /**
     * @test
     */
    public function should_reset_cart_on_checkout()
    {
        $this->session->expects($this->exactly(1))->method('get')->with('cart')->willReturn(new Cart());
        $this->session->expects($this->once())->method('remove')->with('cart');

        $this->manager->proceedCheckout();
    }
}