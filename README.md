## Installation

1. Clone repository
2. Run `composer install` and configure database.
3. Initialize database with `bin/console doctrine:schema:create`
3. Load fixtures with `bin/console doctrine:fixtures:load`
4. Run application with `bin/console server:run`

## Executing tests
 
To execute PHPUnit tests, run `vendor/bin/phpunit` from application's root directory