<?php

namespace Shop\UserBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Shop\ShopBundle\Entity\BuyRequest;

/**
 * @ORM\Entity
 * @ORM\Table(name="`user`")
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\OneToMany(targetEntity="Shop\ShopBundle\Entity\Product", mappedBy="user", cascade={"persist", "remove",
     *                                                               "merge"}, orphanRemoval=true)
     */
    protected $products;

    /**
     * @ORM\OneToOne(targetEntity="Shop\ShopBundle\Entity\Cart", mappedBy="user")
     */
    protected $cart;

    /**
     * @ORM\OneToMany(targetEntity="Shop\ShopBundle\Entity\BuyRequest", mappedBy="user", cascade={"persist", "remove",
     *                                                                  "merge"}, orphanRemoval=true)
     */
    protected $sentBuyRequests;

    /**
     * @ORM\OneToMany(targetEntity="Shop\ShopBundle\Entity\BuyRequest", mappedBy="seller", cascade={"persist",
     *                                                                  "remove", "merge"}, orphanRemoval=true)
     */
    protected $receivedBuyRequests;

    /**
     * @return mixed
     */
    public function getProducts()
    {
        return $this->products;
    }

    /**
     * @return mixed
     */
    public function getCart()
    {
        return $this->cart;
    }

    /**
     * @param mixed $cart
     */
    public function setCart($cart)
    {
        $this->cart = $cart;
    }

    /**
     * @return mixed
     */
    public function getSentBuyRequests()
    {
        return $this->sentBuyRequests;
    }

    /**
     * @param mixed $sentBuyRequests
     */
    public function setSentBuyRequests($sentBuyRequests)
    {
        $this->sentBuyRequests = $sentBuyRequests;
    }

    /**
     * @return mixed
     */
    public function getReceivedBuyRequests()
    {
        return $this->receivedBuyRequests;
    }

    /**
     * @param mixed $receivedBuyRequests
     */
    public function setReceivedBuyRequests($receivedBuyRequests)
    {
        $this->receivedBuyRequests = $receivedBuyRequests;
    }
}