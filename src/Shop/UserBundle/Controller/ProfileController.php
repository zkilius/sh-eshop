<?php

namespace Shop\UserBundle\Controller;

use FOS\UserBundle\Model\UserInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use FOS\UserBundle\Controller\ProfileController as BaseController;

class ProfileController extends BaseController
{
    /**
     * Show the user.
     */
    public function showAction()
    {
        $user = $this->getUser();
        if (!is_object($user) || !$user instanceof UserInterface) {
            throw new AccessDeniedException('This user does not have access to this section.');
        }

        return $this->render(
            'FOSUserBundle:Profile:show.html.twig',
            [
                'user' => $user,
                'products' => $this->getDoctrine()->getManager()->getRepository('ShopBundle:Product')->findBy(
                    ['user' => $user]
                ),
            ]
        );
    }
}
