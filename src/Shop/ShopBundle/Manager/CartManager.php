<?php

namespace Shop\ShopBundle\Manager;

use Doctrine\ORM\EntityManager;
use Shop\ShopBundle\Entity\BuyRequest;
use Shop\ShopBundle\Entity\Cart;
use Shop\ShopBundle\Entity\CartItem;
use Shop\ShopBundle\Entity\Product;
use Shop\UserBundle\Entity\User;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;

class CartManager
{
    /**
     * @var EntityManager
     */
    private $em;
    /**
     * @var TokenStorage
     */
    private $storage;
    /**
     * @var Session
     */
    private $session;

    /**
     * CartManager constructor.
     *
     * @param EntityManager $em
     * @param TokenStorage $storage
     * @param Session $session
     */
    public function __construct(
        EntityManager $em,
        TokenStorage $storage,
        Session $session
    ) {
        $this->em = $em;
        $this->storage = $storage;
        $this->session = $session;
    }

    /**
     * @param Product $product
     * @param         $quantity
     *
     * @return Cart
     */
    public function addItemToCart(Product $product, $quantity)
    {
        $cart = $this->getCart();

        /** @var User $user */
        if ($user = $this->getUser()) {
            $cart->setUser($user);
        }

        if ($this->isProductInCart($product)) {
            $this->updateQuantity($product, $quantity);
        } else {
            $this->insertItem($product, $quantity);
        }

        $this->recalculateCart();

        return $cart;
    }

    /**
     * @return Cart
     */
    public function getCart()
    {
        if (!$this->cartExistsInSession()) {
            $this->session->set('cart', new Cart());
        }

        return $this->session->get('cart');
    }

    /**
     * @return mixed
     */
    private function getUser()
    {
        return $this->storage->getToken()->getUser();
    }

    private function recalculateCart()
    {
        $cart = $this->getCart();
        $total = 0;

        foreach ($cart->getItems() as $item) {
            /** @var CartItem $item */
            $total += $item->getProduct()->getPrice() * $item->getQuantity();
        }

        $cart->setTotal($total);
    }

    /**
     * @param Product $product
     * @param         $quantity
     *
     * @return Cart
     */
    private function insertItem(Product $product, $quantity)
    {
        $cart = $this->getCart();

        $item = new CartItem();
        $item->setCart($cart);
        $item->setProduct($product);
        $item->setQuantity($quantity);

        $cart->addItem($item);

        return $cart;
    }

    /**
     * @param Product $product
     *
     * @return bool
     */
    private function isProductInCart(Product $product)
    {
        return array_key_exists($product->getId(), $this->getCart()->getItems());
    }

    /**
     * @param Product $product
     * @param         $quantity
     */
    private function updateQuantity(Product $product, $quantity)
    {
        $this
            ->getCart()
            ->getItems()[$product->getId()]
            ->setQuantity(
                $this
                    ->getCart()
                    ->getItems()[$product->getId()]->getQuantity() + $quantity
            );
    }

    /**
     * @return bool
     */
    public function cartExistsInSession()
    {
        return $this->session->has('cart');
    }

    public function proceedCheckout()
    {
        $cart = $this->getCart();

        foreach ($cart->getItems() as $item) {
            $this->createBuyRequest($item);
        }

        $this->resetCart();
    }

    /**
     * @param CartItem $item
     */
    private function createBuyRequest(CartItem $item)
    {
        $br = new BuyRequest();

        $product = $this->em->getRepository('ShopBundle:Product')->findOneBy(
            ['id' => $item->getProduct()->getId()]
        );

        $br->setProduct($product);
        $br->setQuantity($item->getQuantity());
        $br->setSeller($product->getUser());
        $br->setUser($this->getUser());

        $this->em->persist($br);
        $this->em->flush();
    }

    private function resetCart()
    {
        $this->session->remove('cart');
    }
}