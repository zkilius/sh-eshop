<?php

namespace Shop\ShopBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Shop\ShopBundle\Entity\Product;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class FrontpageController extends Controller
{
    /**
     * @Route("/", name="homepage")
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {
        $selectedCategory = $request->query->get('category');

        return $this->render(
            '@Shop/frontpage/index.html.twig',
            [
                'products'         => $this
                    ->get('knp_paginator')
                    ->paginate(
                        $this->getProducts($selectedCategory),
                        $request->query->get('page', 1),
                        6
                    ),
                'categories'       => $this->getDoctrine()->getManager()->getRepository('ShopBundle:Category')->findAll(),
                'selectedCategory' => $selectedCategory ?: 0,
            ]
        );
    }

    /**
     * @param         $categoryId
     *
     * @return Product[]
     */
    private function getProducts($categoryId)
    {
        $em = $this->getDoctrine()->getManager();

        if (null !== $categoryId) {
            $category = $em->getRepository('ShopBundle:Category')->findOneBy(['id' => $categoryId]);
            $products = null !== $category ? $em->getRepository('ShopBundle:Product')->findByCategory($category) : [];
        } else {
            $products = $em->getRepository('ShopBundle:Product')->findAll();
        }

        return $products;
    }
}
