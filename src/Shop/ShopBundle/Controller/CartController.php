<?php

namespace Shop\ShopBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Shop\ShopBundle\Entity\Product;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class CartController extends Controller
{
    /**
     * @Route("/cart", name="cart")
     */
    public function indexAction()
    {
        return $this->render('@Shop/cart/index.html.twig');
    }

    /**
     * @Route("/cart/add/{id}", name="add_to_cart")
     * @Method("POST")
     */
    public function addToCartAction(Request $request, Product $product)
    {
        if (!$request->request->has('quantity')) {
            throw new \Exception('Missing quantity parameter!');
        }

        $this->get('shop.manager_cart')->addItemToCart($product, $request->request->get('quantity'));

        return new JsonResponse(['status' => 'success']);
    }

    /**
     * @Route("/cart/checkout", name="checkout")
     * @Method("GET")
     */
    public function checkoutAction()
    {
        $user = $this->getUser();

        if (!$this->isGranted('ROLE_USER')) {
            $this->addFlash('notice', 'You need to sign up or login to complete your order.');
            throw new AccessDeniedException();
        }

        $this->get('shop.manager_cart')->proceedCheckout();

        $this->addFlash('success', 'Your buy request(s) has been submitted!');

        return $this->redirectToRoute('homepage');
    }
}
