<?php

namespace Shop\ShopBundle\Twig;

use Shop\ShopBundle\Manager\CartManager;

class CartExtension extends \Twig_Extension
{
    /**
     * @var CartManager
     */
    private $cartManager;

    /**
     * CartExtension constructor.
     *
     * @param CartManager $cartManager
     */
    public function __construct(CartManager $cartManager)
    {
        $this->cartManager = $cartManager;
    }

    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('getCart', [$this, 'getCart']),
        ];
    }

    public function getCart()
    {
        return $this->getCartManager()->getCart();
    }

    public function getName()
    {
        return 'cart_extension';
    }

    /**
     * @return CartManager
     */
    public function getCartManager()
    {
        return $this->cartManager;
    }
}