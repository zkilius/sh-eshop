<?php

namespace Shop\ShopBundle\Entity;

use Doctrine\ORM\EntityRepository;

class ProductRepository extends EntityRepository
{
    public function findAll()
    {
        return $this
            ->createQueryBuilder("p")
            ->orderBy('p.createdAt', 'DESC');
    }

    /**
     * @param Category $category
     *
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function findByCategory(Category $category)
    {
        return $this
            ->createQueryBuilder("p")
            ->where('p.category = :category')
            ->setParameter('category', $category)
            ->orderBy('p.createdAt', 'DESC');
    }
}
