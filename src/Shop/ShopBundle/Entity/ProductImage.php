<?php

namespace Shop\ShopBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="product_image")
 */
class ProductImage
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string")
     */
    protected $title;

    /**
     * @ORM\Column(type="string")
     */
    protected $thumbnailFileName;

    /**
     * @ORM\Column(type="string")
     */
    protected $fileName;

    /**
     * @ORM\OneToOne(targetEntity="Shop\ShopBundle\Entity\Product", cascade={"all"}, inversedBy="image")
     */
    protected $product;

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getThumbnailFileName()
    {
        return $this->thumbnailFileName;
    }

    /**
     * @param mixed $thumbnailFileName
     */
    public function setThumbnailFileName($thumbnailFileName)
    {
        $this->thumbnailFileName = $thumbnailFileName;
    }

    /**
     * @return mixed
     */
    public function getFileName()
    {
        return $this->fileName;
    }

    /**
     * @param mixed $fileName
     */
    public function setFileName($fileName)
    {
        $this->fileName = $fileName;
    }

    /**
     * @return mixed
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @param mixed $product
     */
    public function setProduct($product)
    {
        $this->product = $product;
    }

    public function __toString()
    {
        return $this->fileName;
    }
}