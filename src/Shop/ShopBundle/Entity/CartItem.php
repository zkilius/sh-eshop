<?php

namespace Shop\ShopBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Shop\UserBundle\Entity\User;

/**
 * @ORM\Entity
 * @ORM\Table(name="cart_item")
 */
class CartItem
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="Shop\ShopBundle\Entity\Cart", cascade={"all"}, fetch="EAGER", inversedBy="items")
     */
    protected $cart;

    /**
     * @ORM\OneToOne(targetEntity="Shop\ShopBundle\Entity\Product")
     */
    protected $product;

    /**
     * @ORM\Column(type="integer")
     */
    protected $quantity;

    /**
     * @return mixed
     */
    public function getCart()
    {
        return $this->cart;
    }

    /**
     * @param mixed $cart
     */
    public function setCart($cart)
    {
        $this->cart = $cart;
    }

    /**
     * @return mixed
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @param mixed $product
     */
    public function setProduct($product)
    {
        $this->product = $product;
    }

    /**
     * @return mixed
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * @param mixed $quantity
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;
    }
}