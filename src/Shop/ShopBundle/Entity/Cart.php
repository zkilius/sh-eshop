<?php

namespace Shop\ShopBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Shop\UserBundle\Entity\User;

/**
 * @ORM\Entity
 * @ORM\Table(name="cart")
 */
class Cart
{
    public function __construct()
    {
        $this->items = [];
    }

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\OneToOne(targetEntity="Shop\UserBundle\Entity\User", cascade={"all"}, fetch="EAGER", inversedBy="cart")
     */
    protected $user;

    /**
     * @ORM\OneToMany(targetEntity="Shop\ShopBundle\Entity\CartItem", mappedBy="cart", cascade={"persist", "remove", "merge"},
     *                                        orphanRemoval=true)
     */
    protected $items;

    /**
     * @ORM\Column(type="float")
     */
    protected $total;

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * @param CartItem $item
     */
    public function addItem(CartItem $item)
    {
        $this->items[$item->getProduct()->getId()] = $item;
    }

    /**
     * @return mixed
     */
    public function getItems()
    {
        return $this->items;
    }

    /**
     * @param mixed $items
     */
    public function setItems($items)
    {
        $this->items = $items;
    }

    /**
     * @return mixed
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * @param mixed $total
     */
    public function setTotal($total)
    {
        $this->total = $total;
    }
}