<?php

namespace Shop\ShopBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Shop\ShopBundle\Entity\Category;

class LoadCategoriesData implements FixtureInterface
{
    private $categories = ['Women Clothes', 'Men Clothes', 'Children Clothes'];

    public function load(ObjectManager $manager)
    {
        foreach ($this->categories as $categoryName) {
            $category = new Category();
            $category->setTitle($categoryName);

            $manager->persist($category);
        }

        $manager->flush();
    }
}